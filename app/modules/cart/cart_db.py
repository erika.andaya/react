from bson.objectid import ObjectId
from decimal import Decimal

class CartDB:
    def __init__(self, conn):
        self.conn = conn

    # def get_cart_items(self, username):
    #     return self.conn.find({'username':username})

    def add_item_to_cart(self, item_id, item, price, quantity, username):
        self.conn.insert({'item_id':ObjectId(item_id),'item':item, 'price': price, 'quantity': quantity, 'username' : username})


    def get_cart_items(self, username):
        return self.conn.aggregate( [ {"$match": { "username" : username} } , { "$group": { "_id": "$item_id", "quantity": {"$sum": "$quantity"}, "total_price": { "$sum": {"$multiply": ["$price", "$quantity"]}}, "price": {"$first": "$price"} } } , { "$lookup": { "from": "inventory", "localField": "_id", "foreignField": "_id", "as": "item_details" } } ,{'$unwind': '$item_details'} ] ) 

    def get_total(self, username):
        return self.conn.aggregate([{"$match": {"username" : username}} , { "$group": { "_id": "$username", "total": { "$sum": {"$multiply": ["$price", "$quantity"]}}}}])

    def remove(self):
        self.conn.remove({})







