from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from ..forms import add_to_cart_form
from flask import request
from decimal import Decimal
from bson import json_util, ObjectId
import json



mod = Blueprint('cart', __name__)


@mod.route('/cart_items', methods=['GET', 'POST'])
def list_cart_items():
	if request.method == 'GET':
		return render_template('cart/cart_items_list.html')
	else:
		cart = g.cartdb.get_cart_items(session['username'])
		json_cart = []
		for i in cart:
			json_cart.append(i)
		return json.dumps(json_cart, sort_keys=True, indent=4, default=json_util.default)



@mod.route('/cart_form', methods=['POST'])
def go_to_add_form():
	item = request.form['item']
	price = request.form['price']
	item_id = request.form['item_id']
	return render_template('cart/add_to_cart.html', item = item, price=price, item_id = item_id)


@mod.route('/add_to_cart', methods=['POST'])
def add_to_cart():
	item= request.form['item']
	price= request.form['price']
	quantity = request.form['quantity']
	item_id = request.form['item_id']
	g.cartdb.add_item_to_cart(item_id , item, float(price), int(quantity), session['username'])
	return redirect('/cart/cart_items')





