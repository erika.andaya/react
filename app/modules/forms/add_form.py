from wtforms import Form
from wtforms import TextField
from wtforms import validators
from wtforms import FloatField

class AddItemForm(Form):
    item = TextField('Item', [validators.Required()])
    price = FloatField('Price', [validators.Required()])