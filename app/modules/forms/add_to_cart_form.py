from wtforms import Form
from wtforms import validators
from wtforms import IntegerField

class AddToCartForm(Form):
    quantity = IntegerField('Quantity', [validators.Required()])