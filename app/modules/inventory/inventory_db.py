from bson.objectid import ObjectId

class InventoryDB:
    def __init__(self, conn):
        self.conn = conn

    def get_items(self):
        return self.conn.find({})

    def add_item(self, item, price):
        self.conn.insert({'item':item, 'price': price})

    def remove(self):
        self.conn.remove({})


