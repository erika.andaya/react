from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from ..forms import add_form
from ..forms import add_to_cart_form
from bson import json_util, ObjectId
import json


mod = Blueprint('inventory', __name__)

@mod.route('/items', methods=['GET', 'POST'])
def list_items():
	if request.method == 'POST':
		inventory = g.inventorydb.get_items()
		json_inventory = []
		for i in inventory:
			json_inventory.append(i)
		return json.dumps(json_inventory, sort_keys=True, indent=4, default=json_util.default)
	return render_template('inventory/items_list.html')


@mod.route('/add_item', methods=['GET', 'POST'])
def add_item():
	if request.method == 'POST':
		item = request.form['item']
		price = request.form['price']
		if validateValue(price):
			g.inventorydb.add_item(item, price)
			return redirect('/inventory/items')
		else:
			flash('Invalid Amount!', 'alert')
	return render_template('inventory/add_item.html')


def validateValue(value):
	try:
		val = int(value)
		return True
	except ValueError:
		try:
			val = float(value)
			return True
		except ValueError:
			return False






	