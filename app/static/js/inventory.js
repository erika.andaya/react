var Inventory = React.createClass({
  getInitialState: function() {
    return {data: []};
  },
  loadInventoryFromServer: function() {
    $.ajax({
      url: this.props.url,
      type: 'POST',
      success: function(data) {
        console.log(JSON.parse(data))
        this.setState({data: JSON.parse(data)});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadInventoryFromServer()
    console.log(this.state.data)
  },
  renderData: function(d, i){
    var id=[]
    id.push(d._id)
    var oid = id[0].$oid
    return (<tr>
              <td>{d.item}</td>
              <td>{d.price}</td>
              <td>
                <form method='post' action="/cart/cart_form">
                    <input class='form-control' type='hidden' name='item_id' value= {oid}/>
                    <input class='form-control' type='hidden' name='item' value= {d.item}/>
                    <input class='form-control' type='hidden' name='price' value= {d.price}/>
                    <input className='btn btn-default btn-sm' type='submit' value = 'Add to Cart' />
                </form>
              </td>
            </tr>)
  },
  render: function() {
    return (
      <div >
        <h1>Inventory Items</h1>
        <table className="table">
          <thead>
            <tr>
            <th>Item</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
          </thead>
              <tbody>{this.state.data.map(this.renderData)}</tbody>
        </table>
          
      </div>
    );
  }
});


 
ReactDOM.render(
  <Inventory url="/inventory/items"/>,
  document.getElementById('content')
);
