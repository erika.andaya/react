var Cart = React.createClass({
  getInitialState: function() {
    return {data: []};
  },
  loadCartFromServer: function() {
    $.ajax({
      url: this.props.url,
      type: 'POST',
      success: function(data) {
        console.log(JSON.parse(data))
        this.setState({data: JSON.parse(data)});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadCartFromServer()
    console.log(this.state.data)
  },
  renderData: function(d, i){


    var i=[]
    i.push(d.item_details)
    var item_name = i[0].item



    return (<tr>
              <td>{item_name}</td>
              <td>{d.price}</td>
              <td>{d.quantity}</td>
              <td>{d.total_price}</td>
            </tr>)
  },
  render: function() {
    return (
      <div>
        <h1>Cart Items</h1>
        <table className="table">
        <thead>
          <tr>
            <th>Item</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Sub Total</th>
          </tr>
          </thead>
              <tbody>{this.state.data.map(this.renderData)}</tbody>
        </table>
          
      </div>
    );
  }
});
 
ReactDOM.render(
  <Cart url="/cart/cart_items" />,
  document.getElementById('content')
);