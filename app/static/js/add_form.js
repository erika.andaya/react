
class InventoryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {item: ''};
    this.state = {price: ''};

    this.handleChange = this.handleChange.bind(this);
  
  }

  handleChange(event) {
    this.setState({item: event.target.item});
    this.setState({price: event.target.price});
  }


  render() {
    return (
      <form className='form-signin' method='post' action="/inventory/add_item">
        <h2 className="form-signin-heading">Item Details</h2>
        <dl>
        <input className='form-control' type='text' required='true' placeholder="Item Name" name='item' value= {this.state.item} onChange={this.handleChange} />
        </dl>
        <dl>
        <input className='form-control' type='text' required='true' name='price' placeholder="Price"  value= {this.state.price} onChange={this.handleChange} />
        </dl>
        <input className='btn btn-lg btn-primary btn-block' type='submit' value="Add Item to Inventory" />
      </form>
    );
  }
}

ReactDOM.render(
  <InventoryForm url="/inventory/add_item" />,
  document.getElementById('content')
);