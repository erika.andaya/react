
class CartForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {quantity: ''};

    this.handleChange = this.handleChange.bind(this);
  
  }

  handleChange(event) {
    this.setState({quantity: event.target.quantity});

  }


  render() {
    return (
      <div>
      <form className='form-signin' method='post' action="/cart/add_to_cart">
        <h2 className="form-signin-heading">Enter Quantity</h2>
         <dl>
        <input className='form-control' type='hidden' name='item_id'value= {this.props.item_id} onChange={this.handleChange} />
        </dl>
         <dl>
        <input className='form-control' type='text' name='item'value= {this.props.item} onChange={this.handleChange} />
        </dl>
        <dl>
        <input className='form-control' type='text' name='price' value= {this.props.price} onChange={this.handleChange} />
        </dl>
        <dl>
        <input className='form-control' type='number' required='true' name='quantity' placeholder="Quantity"  value= {this.state.quantity} onChange={this.handleChange} />
        </dl>
        <input className='btn btn-lg btn-primary btn-block' type='submit' value="Add Item to Cart" />
      </form>
      </div>
    );
  }
}
ReactDOM.render(
  <CartForm url="/cart/cart_form"  item = {item} price = {price} item_id = {item_id}/>,
  document.getElementById('content')
);


