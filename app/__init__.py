from flask import Flask
from flask import g

from modules import default
from modules import inventory
from modules import cart
from modules import users
from modules import util

from pymongo import MongoClient

app = Flask(__name__)

app.session_interface = util.MongoSessionInterface(db='sessions')

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.inventorydb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = users.UserDB(conn=mainDb.users)
    g.inventorydb = inventory.InventoryDB(conn = mainDb.inventory)
    g.cartdb = cart.CartDB(conn = mainDb.cart)
    
@app.teardown_request
def teardown_request(exception):
    inventorydb = getattr(g, 'inventorydb', None)
   # if inventorydb is not None:
    #    inventorydb.close()
    cartdb = getattr(g, 'cartdb', None)
   # if cartdb is not None:
       # cartdb.close()
    userdb = getattr(g, 'userdb', None)
    if userdb is not None:
        userdb.close()

app.register_blueprint(default.mod)
app.register_blueprint(inventory.mod, url_prefix="/inventory")
app.register_blueprint(cart.mod, url_prefix="/cart")